# Change Log
Alterações realizadas no plugin.


## [1.0.1]
### Changed
- Torna o item de menu "Acesso Rápido" visível a todos os usuários logados.

## [1.0]
### Added
- Versão inicial
